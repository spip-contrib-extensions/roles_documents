<?php

/**
 * Gestion du formulaire d'édition des rôles des liens d'un document
 *
 * `#FORMULAIRE_EDITER_ROLES_DOCUMENT{2,article,3}`
 * pour editer les roles du document 2 lié à l'article 3
 *
 * @package SPIP\Formulaires
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Chargement du formulaire d'édition des rôles des liens d'un document
 *
 * @param int $id_document
 *     Identifiant du document
 * @param string $objet
 *     Type d'objet lié
 * @param int $id_objet
 *     Identifiant de l'objet lié
 * @param array $options
 *     Tableaux associatif d'options
 *     ajaxReload : sélecteur CSS d'un bloc à recharger
 * @return array
 */
function formulaires_editer_roles_document_charger_dist($id_document, $objet, $id_objet, $options = []) {

	// charger les valeurs de editer_liens
	$editer_liens_charger = charger_fonction('charger', 'formulaires/editer_liens');
	$valeurs = $editer_liens_charger('spip_documents', $objet, $id_objet) ?: [];

	// Renvoyer aussi id_document
	$valeurs['id_document'] = (int) $id_document;

	// Les rôles attribués
	$roles_attribues =  [];
	$roles_presents = roles_documents_presents_sur_objet($objet, $id_objet, $id_document);
	$titrer_role = chercher_filtre('role');
	foreach ($roles_presents['attribues'] as $role) {
		$roles_attribues[$role] = $titrer_role($role, 'document');
	}
	$valeurs['roles_attribues'] = $roles_attribues;
	
	// Éditable seulement s'il y a des attribuables OU plusieurs attribués (s'il n'y en a qu'un on ne doit pas pouvoir l'enlever), sinon juste affichage
	$valeurs['editable'] = count($roles_presents['attribuables']) || count($roles_presents['attribues']) > 1;

	return $valeurs;
}

/**
 * Traiter le post des informations d'édition des rôles des liens d'un document
 *
 * On effectue les traitements par défaut de editer_liens, sauf qu'on s'assure de conserver l'unicité des rôles principaux (= logos)
 *
 * @param int $id_document
 *     Identifiant du document
 * @param string $objet
 *     Type d'objet lié
 * @param int $id_objet
 *     Identifiant de l'objet lié
 * @param array $options
 *     Tableaux associatif d'options
 *     ajaxReload : sélecteur CSS d'un bloc à recharger
 * @return array
 */
function formulaires_editer_roles_document_traiter_dist($id_document, $objet, $id_objet, $options = []) {

	include_spip('inc/editer_liens'); // inclus également les rôles
	include_spip('base/objets');
	$res = [];
	$done = false; // flag pour savoir si on fait les traitements génériques de editer_liens
	$id_table_objet = id_table_objet($objet);

	// Récupérer les rôles principaux (=logos) de l'objet
	$roles = roles_documents_presents_sur_objet($objet, $id_objet, 0, true);

	// Action effectuée
	$ajouter = _request('ajouter_lien');
	$supprimer = _request('supprimer_lien');

	// Ajouter : vérification de l'unicité du rôle
	if ($ajouter) {
		$role = retrouver_action_role_document($ajouter);
		// Si c'est rôle principal (=logo) déjà attribué, on met à jour le lien existant
		if (
			in_array($role, $roles['attribues'])
			and $document_present = objet_trouver_liens(['document' => '*'], [$objet => $id_objet], ['role=' . sql_quote($role)])
		) {
			$update = sql_updateq(
				'spip_documents_liens',
				[
					'id_document' => $id_document,
				],
				[
					'id_document=' . intval($document_present[0]['id_document']),
					'objet=' . sql_quote($objet),
					'id_objet=' . intval($id_objet),
					'role=' . sql_quote($role),
				]
			);
			$done = true;
		}
	}

	// Si on a fini nos traitements
	if ($done) {
		$res['editable'] = true;

	// Sinon traitements génériques de editer_liens
	} else {
		$editer_liens_traiter = charger_fonction('traiter', 'formulaires/editer_liens');
		$res = $editer_liens_traiter('spip_documents', $objet, $id_objet);
	}

	// recharger un ou plusieurs blocs après modification des roles
	// ajaxReload est un sélecteur css, tel que '#documents'
	if (!empty($options['ajaxReload'])) {
		$js = '<script type="text/javascript">';
		$js .= 'if (window.jQuery) jQuery("' . $options['ajaxReload'] . '").ajaxReload();';
		$js .= '</script>';
		if (isset($res['message_erreur'])) {
			$res['message_erreur'] .= $js;
		} elseif (isset($res['message_ok'])) {
			$res['message_ok'] .= $js;
		} else {
			$res['message_ok'] = $js;
		}
	}

	return $res;
}


/**
 * Fonction privée pour retrouver le rôle sélectionné dans une action.
 *
 * @param array $action
 *     Valeur du bouton = arguments séparés par des tirets
 *     document-id_document-objet-id_objet-role
 * @return string|false
 *     Le rôle de l'action, sinon false
 */
function retrouver_action_role_document($action) {

	$role = false;
	$action = array_flip($action);
	$action = array_shift($action);
	if (
		is_array($arguments = explode('-', $action))
		and count($arguments) === 5
	) {
		$role = $arguments[4];
	}

	return $role;
}
