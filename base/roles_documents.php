<?php

/**
 * Plugin Rôles de documents
 * (c) 2015
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclarer la liste des rôles
 *
 * @note
 * L'API des rôles impose de donner un rôle par défaut : on ne peut pas dissocier des objets sans rôle.
 *
 * @param array $tables
 * 		Description des tables
 * @return array
 * 		Description complétée des tables
 */
function roles_documents_declarer_tables_objets_sql($tables) {
	include_spip('inc/config');

	// On déclare un rôle `document` par défaut (faute de pouvoir mettre un rôle vide)
	$colonne = 'role';
	$titres = [
		'document' => 'roles_documents:role_document',
		'logo' => 'roles_documents:role_logo',
		'logo_survol' => 'roles_documents:role_logo_survol',
	];
	$principaux = ['logo', 'logo_survol'];
	$choix = ['document'];

	// Si les logos sont activés, on les propose dans les choix possibles
	$config_logos = [
		'logo'  => lire_config('activer_logos') == 'oui',
		'logo_survol' => lire_config('activer_logos_survol') == 'oui',
	];
	$roles_logos = array_keys(array_filter($config_logos));
	foreach ($roles_logos as $role) {
		$choix[] = $role;
	}

	// Déclaration pour tous les objets
	$objets = [
		'*' => [
			'choix'      => $choix,
			'defaut'     => reset($choix),
			'principaux' => $principaux,
		]
	];

	// On merge avec les éventuelles déclarations de rôles déjà présentes
	$tables['spip_documents']['roles_colonne'] = $tables['spip_documents']['roles_colonne'] ?? $colonne;
	$tables['spip_documents']['roles_titres'] = array_merge(
		$tables['spip_documents']['roles_titres'] ?? [],
		$titres
	);
	$tables['spip_documents']['roles_objets'] = array_merge(
		$tables['spip_documents']['roles_objets'] ?? [],
		$objets
	);

	return $tables;
}

/**
 * Ajouter la colonne de rôle sur la table de liens
 *
 * @param array $tables
 * 		Description des tables auxiliaires
 * @return array
 * 		Description complétée
**/
function roles_documents_declarer_tables_auxiliaires($tables) {
	$tables['spip_documents_liens']['field']['role']        = "varchar(30) NOT NULL DEFAULT ''";
	$tables['spip_documents_liens']['key']['PRIMARY KEY']   = 'id_document,id_objet,objet,role';
	return $tables;
}
