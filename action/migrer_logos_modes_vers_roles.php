<?php

/**
 * Action : Convertir les logos traditionnels en logos avec rôle
 *
 * @plugin     Rôles de documents
 * @copyright  2015-2022
 * @author     tcharlss, RastaPopoulos
 * @licence    GNU/GPL
 * @package    SPIP\Roles_documents\Action
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Migre les documents avec un mode logo en documents avec le rôle logo équivalent
 *
 * On peut rejouer l'action plusieurs fois pour migrer progressivement.
 *
 * @return array
 *     Tableau avec la liste des documents ayant été migrés pour chaque rôle
 *     array<list logo, list logo_survol>
 */
function action_migrer_logos_modes_vers_roles_dist() {

	// On retourne le nombre de docs migrés pour chaque rôle
	$migration = [
		'logo'  => [],
		'logo_survol' => [],
	];

	// Il faut être webmestre
	// TODO : permettre d'appeler depuis spip-cli
	//$securiser_action = charger_fonction('securiser_action', 'inc');
	include_spip('inc/autoriser');
	if (autoriser('webmestre')) {
		include_spip('roles_documents_administrations');
		$migration = roles_documents_maj_migrer_logos_modes_vers_roles();
	} else {
		spip_log('Action migrer logos mode vers rôle : pas autorisé', 'roles_documents' . _LOG_ERREUR);
	}

	return $migration;
}
