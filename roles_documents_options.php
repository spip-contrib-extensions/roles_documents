<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Surcharge de Bigup car ici c'est le form de documents
 * @param array $args
 * @param \Spip\Bigup\Formulaire $formulaire
 * @return \Spip\Bigup\Formulaire
 */
function inc_bigup_medias_formulaire_editer_logo($args, $formulaire) {
	// Les mêmes options que le code d'origine pour les logos
	$options = [
		//~ 'accept' => bigup_get_accept_logos(),
		'previsualiser' => true,
		'input_class' => 'bigup_logo',
	];
	if (intval($args['args'][1]) or $args['args'][0] !== 'site') {
		$options['drop-zone-extended'] = '#navigation';
	}

	// Mais pas contre c'est le form classique des documents
	$formulaire->preparer_input(
		'fichier_upload[]',
		$options
	);
	$formulaire->inserer_js('bigup.logos.js');

	return $formulaire;
}

/**
 * Trouve le bon rôle à utiliser suivant un état ou rôle demandé qui peut être les anciens moches "on" et "off"
 *
 * @param string $objet Objet
 *     Objet dont on cherche le bon rôle correspondant à l'état demandé
 * @param string $mode
 *     État ancien "on" ou "off", ou nouveau rôle directement
 * @return string
 *     Retourne le bon rôle à utiliser
 */
function roles_documents_compatibilite_anciens_etats(string $objet, string $etat) {
	// Vieux états/modes
	$etats = ['on', 'off'];

	// Retrouver les rôles principaux pour cet objet
	$roles = roles_presents('document', $objet);
	$roles_principaux = !empty($roles['roles']['principaux']) ? $roles['roles']['principaux'] : ['logo', 'logo_survol'];

	// Correspondance vieux états / rôles principaux (voir @note)
	if ($etat == $etats[0] and isset($roles_principaux[0])) {
		$role = $roles_principaux[0];
	} elseif ($etat == $etats[1] and isset($roles_principaux[1])) {
		$role = $roles_principaux[1];
	} else {
		// Sinon c'est un nouveau role directement (ou un truc qui n'existe pas et qui ne sortera rien)
		$role = $etat;
	}

	return $role;
}

/**
 * Cherche le logo d'un élément d'objet / SURCHARGE -dist du core
 *
 * @global array formats_logos Extensions possibles des logos
 * @uses type_du_logo()
 *
 * @param int $id
 *     Identifiant de l'objet
 * @param string $_id_objet
 *     Nom de la clé primaire de l'objet
 * @param string $mode
 *     Mode de survol du logo désiré (on ou off)
 * @return array
 *     - Liste (chemin complet du fichier, répertoire de logos, nom du logo, extension du logo, date de modification[, doc])
 *     - array vide aucun logo trouvé.
 **/
function inc_chercher_logo($id_objet, $_id_objet, $mode = 'on') {
	include_spip('inc/chercher_logo');
	$mode = preg_replace(',\W,', '', $mode);
	$objet = objet_type($_id_objet);

	if ($mode) {
		include_spip('inc/roles');

		// On cherche le bon rôle à utiliser
		$role = roles_documents_compatibilite_anciens_etats($objet, $mode);

		if (
			$document = sql_fetsel(
				'doc.*, lien.role',
				'spip_documents AS doc' .
				' INNER JOIN spip_documents_liens AS lien' .
				' ON lien.id_document = doc.id_document',
				[
				'objet = ' . sql_quote($objet),
				'id_objet = ' . intval($id_objet),
				'role = ' . sql_quote($role),
				//sql_in('role', $roles_principaux), // too much ?
				]
			)
		) {
			include_spip('inc/documents');
			
			// Si on a mis explicitement une vignette différente pour ce document
			if (
				$document['id_vignette'] > 0
				and $vignette = sql_getfetsel('fichier', 'spip_documents', 'id_document = '.$document['id_vignette'])
			) {
				$chemin = get_spip_doc($vignette);
			}
			// Sinon on renvoie le fichier du document
			else {
				$chemin = get_spip_doc($document['fichier']);
			}
			
			if (file_exists($chemin)) {
				$logo = [$chemin, _DIR_IMG, $document['titre'], $document['extension'], @filemtime($chemin), $document];
			} else {
				$logo = [];
			}
			return $logo;
		}
	}

	# coherence de type pour servir comme filtre (formulaire_login)
	return [];
}
