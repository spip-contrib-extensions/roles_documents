<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_modifier_roles' => 'Modifier',
	'bouton_iconifier_logo' => 'Convertir en document',

	// C
	'champ_role_label' => 'Rôle',
	'champ_roles_label' => 'Rôles',

	// M
	'media_logos' => 'Logos',

	// R
	'role_logo' => 'Logo',
	'role_logo_survol' => 'Logo de survol',
	'role_document' => 'Document',

	// T
	'titre_ajouter_doc_principal' => 'Ajouter un document principal',
	'titre_ajouter' => 'Ajouter',
);
