<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;

class RolesDocumentsMigrerLogos extends Command {
	protected function configure() {
		$this
			->setName('roles_documents:migrer_logos')
			->setDescription('Migrer les logos en rôle de documents logo.')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		include_spip('roles_documents_administrations');
		$migration = roles_documents_maj_migrer_logos_modes_vers_roles();
		
		foreach ($migration as $role => $ids) {
			$nb = count($ids);
			$this->io->text("Migration pour le rôle $role : <info>$nb documents</info>");
		}
		
		return self::SUCCESS;
	}
}
