<?php

/**
 * Plugin Rôles de documents
 * (c) 2015
 * Licence GNU/GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation du plugin et de mise à jour.
**/
function roles_documents_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	// 1ère installation
	$maj['create'] = [
		// supprimer la clé primaire actuelle pour pouvoir en changer en ajoutant la colonne rôle
		['sql_alter', 'TABLE spip_documents_liens DROP PRIMARY KEY'],
		// ajout de la colonne role
		['maj_tables', ['spip_documents_liens']],
		// la nouvelle colonne est la, mettre sa nouvelle clé primaire
		['sql_alter', 'TABLE spip_documents_liens ADD PRIMARY KEY (id_document,id_objet,objet,role)'],
		// Mettre un rôle 'document' par défaut aux liens dépourvus de rôle
		['sql_update', 'spip_documents_liens', ['role' => sql_quote('document')], 'role=' . sql_quote('')],
		// Migration des logos SPIP 4 en docs avec rôles
		['roles_documents_maj_migrer_logos_modes_vers_roles', true],
	];

	// 1.0.1 : mettre un rôle 'document' par défaut aux liens dépourvus de rôle
	$maj['1.0.1'] = [
		['sql_update', 'spip_documents_liens', ['role' => sql_quote('document')], 'role=' . sql_quote('')],
	];

	// 1.1.2 : migration des logos SPIP 4 en docs avec rôles
	$maj['1.1.2'] = [
		['roles_documents_maj_migrer_logos_modes_vers_roles', true],
		['sql_update', 'spip_documents_liens', ['objet' => sql_quote('site'), 'id_objet' => 0], 'objet="site_spip"'],
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin.
 */
function roles_documents_vider_tables($nom_meta_base_version) {
	// TODO : migration des logos à leur état original

	// tant qu'il existe des doublons, on supprime une ligne doublonnée
	// sinon on ne pourra pas modifier la cle primaire ensuite
	// cet algo est certainement a optimiser
	while (
		$doublons = sql_allfetsel(
			['id_document', 'id_objet', 'objet', 'role'],
			['spip_documents_liens'],
			'',
			'id_document,id_objet,objet',
			'',
			'',
			'COUNT(*) > 1'
		)
	) {
		foreach ($doublons as $d) {
			$where = [];
			foreach ($d as $cle => $valeur) {
				$where[] = "$cle=" . sql_quote($valeur);
			}
			sql_delete('spip_documents_liens', $where);
		}
	}

	// supprimer la clé primaire, la colonne rôle, et remettre l'ancienne clé primaire
	sql_alter('TABLE spip_documents_liens DROP PRIMARY KEY');
	sql_alter('TABLE spip_documents_liens DROP COLUMN role');
	sql_alter('TABLE spip_documents_liens ADD PRIMARY KEY (id_document,id_objet,objet)');

	effacer_meta($nom_meta_base_version);
}

/**
 * Migration des modes logo vers des rôles
 *
 * @return array
 */
function roles_documents_maj_migrer_logos_modes_vers_roles($timeout = null) {
	// Si on demande à utiliser la constante prédéfinie
	if ($timeout === true) {
		$time = _TIME_OUT;
	}
	
	$migration = [
		'logo'  => [],
		'logo_survol' => [],
	];

	// Les documents logos ont un mode `logoon` ou `logooff`, et sont liés aux objets avec un lien unique.
	// On garde la même nomenclature pour les rôles histoire de simplifier.
	$modes = [
		'logoon' => 'logo',
		'logooff' => 'logo_survol',
	];
	$total = 0;
	foreach ($modes as $mode => $role) {
		// Nb : on prend même ceux qui auraient éventuellement déjà un rôle logo,
		// pour être sûr de reset le mode dans ce cas.
		while (
			$documents = sql_allfetsel(
				'D.id_document',
				'spip_documents as D INNER JOIN spip_documents_liens L ON L.id_document=D.id_document',
				'D.mode = ' . sql_quote($mode),
				'',
				'',
				'0,1000'
			)
		) {
			$ids_documents = array_column($documents, 'id_document');
			$ids_documents = array_map('intval', $ids_documents);
			// On ajoute le rôle logo…
			sql_updateq(
				'spip_documents_liens',
				['role' => $role],
				sql_in('id_document', $ids_documents)
			);
			// …et on reset le mode
			sql_updateq(
				'spip_documents',
				['mode' => 'image'], // ce sont forcément des images
				sql_in('id_document', $ids_documents)
			);
			// TODO : vérifier que sql_updateq s'est bien passé avant des les compter dans les migrés
			$nb = count($ids_documents);
			spip_log("roles_documents_maj_migrer_logos_modes_vers_roles: $nb logos mode $mode migrés", 'maj' . _LOG_INFO_IMPORTANTE);
			$migration[$role] += $ids_documents;
			$total += $nb;
			// si on est en mise à jour de base gérer le timeout
			if (!is_null($timeout) and time() > $timeout) {
				return $migration;
			}
		}
	}

	spip_log("roles_documents_maj_migrer_logos_modes_vers_roles: $total logos migrés", 'roles_documents' . _LOG_INFO_IMPORTANTE);

	return $migration;
}
